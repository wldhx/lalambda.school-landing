function getCell(sheet: string, cell: string): Promise<string> {
    // this is a read-only Google Sheets API key
    const key = 'AIzaSyBdwI1hsx-CihMXIvarnS74WkmsaTeHvS0';

    return fetch(
        `https://sheets.googleapis.com/v4/spreadsheets/${sheet}/values/${cell}?fields=values&key=${key}`
    ).then((x) => x.json()).then((x) => x.values[0][0]);
}

export function getFinancesCell(cell: string) {
    return getCell('1msH1sFtUGASc4Db125O5nL6Mt_HuETfDVC3WLIFhPok', cell);
}
