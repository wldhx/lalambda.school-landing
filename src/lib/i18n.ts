export function invertLocale(locale: string): string {
    if (locale === 'en') return 'ru';
    return 'en';
}

export default {
    en: {
        headline: ['Formal methods', 'Machine learning', 'Music theory', 'FPGA', 'Cryptography', 'Systems programming', 'Functional programming', 'Art'],
        gamarjoba: 'Gamarjoba',
        tagline: 'A summer school on advanced programming and',
        tagline_contemporary: 'contemporary art',
        headline_2: (count: string) => `A peer-to-peer assembly ${count ? `of ${count} ` : ''}for hack and fun.`,
        duration_info: ['40h of courses', '30h of art'],
        time: ['15-23 July'],
        place: ['near Tbilisi'],

        pic_titles: ['Pick a full-time course', 'Check out art electives', 'Come and play'],

        names_and_faces: 'Names & Faces',

        courses_header: 'Courses (tentative)',
        courses: [
            {
                title: 'Machine learning greatest hits',
                desc:
                    `
This course fits both R&D researchers from other fields who are curious about what's happening, but have no interest in distinguishing kittens from dogs using \`fit_predict()\` and true machine learners who feel entrenched in their subdomain (CV, for example) and want to see what's happening in other areas. (Presenting a cool article is highly encouraged!)

The course will be conducted by several seminarists with expertise in their particular fields, and it will work like this: we will read the articles, discuss them together and decipher them until all the aspects that interest us become clear, and weave narratives about where things are going or have gone.

Unlike other courses at the school, which may teach how to do something hands-on through lectures and assignments, this one is aimed more at conceptual understanding (we will be reading and maybe deriving formulas, but we won't be writing much code) and peer-to-peeredness (if you're proficient in X — do tell, it's a seminar).
`,
            },
            {
                title: 'Purescript',
                desc:
                    `
PureScript is a purely functional language borrowing many concepts from Haskell but designed for compiling into JavaScript.

**In this course, you'll get:**

- a brief introduction to functional programming
- a bit of personal practice
- a collective exploration of the language — writing our own implementation of the Virtual DOM

**The course will be divided into two consecutive parts:**

1. Fundamentals: an introduction to PureScript and FP. Pure functions, effects, monads, language syntax.
2. Practice: hacking VDOM. We'll take a look inside the Halogen framework. Implementation of our own diff and patch. Tree traversal. Interaction of PureScript with JavaScript.

You can start with the first part to dive in gently, or skip it to plunge into VDOM from the get-go.
`,
            },
            {
                title: 'Focused Practice on Gate Arrays',
                desc:
                    `
Programming is so intricately bound to the semantics of the von Neumann machine that many simply can't envision an algorithm being implemented by anything other than a chain of instructions, multiple cores with multiple chains, or microcode with signal control generation from a quasi-instruction chain.

In fact, the CPU and all its variants are only a very specific instance of what can be accomplished in hardware. To grasp what can be done in hardware, one must not view it in programming terms (loops, function calls, etc.), but rather through hardware concepts: combinational clouds and state elements, pipelines and queues, clock cycles and within-cycle delays, which determine the clock frequency.

Over the last 30 years, chip designers have not been drawing digital diagrams manually on a screen, but have been synthesizing them with hardware description language (HDL) code, such as Verilog or VHDL. This code may resemble programming code, but it has different semantics, which we'll explore in this course. Once developed, this code can be synthesized into a physical chip design (how new designs are made) or converted into a logical element graph and flashed into FPGA memory.

**FPGA is a chip where transistor connections are programmable.**

By flashing it with this code, we get a hardware accelerator that is slower than a specifically crafted, task-focused circuit, but for many tasks, it is faster/more energy-efficient than a conventional CPU with equivalent software. This is how new processors and chips are prototyped, and hardware accelerators are developed.

In addition to simple FPGA labs, we'll demonstrate how to build a RISC-V processor and use FPGA for sound processing. We'll also look at the Open Lane ASIC design tool, a free and open-source alternative to commercial packages from Synopsys and Cadence.

Finally, we will connect other school topics such as formal methods and constraint solvers with their use in electronic companies. In these companies, temporal logic proofs are applied for pipeline verification, and constraint solvers are used for generating non-contradictory pseudo-random transactions during functional verification through simulation.

**Course Plan by days:**

1. Combinatorial logic - computations without cycles. Hardware description languages, ASIC and FPGA routes, as well as life in the electronics industry.
2. Sequential logic, clock signal, and finite state machines. Application for music recognition.
3. The two sides of a processor - architecture and microarchitecture. Analysis of the minimalist schoolRISCV core.
4. Unstoppable force: why a specialized ASIC is orders of magnitude faster than a CPU. Pipelines, queues, embedded memory blocks, and clock frequency issues. Applications for GPUs, network chips, and machine learning accelerators.
5. Formal verification, using C++ for FPGA project development, and for dessert: an example breakdown of a processor inside an FPGA, connected to VGA and playing Tetris.

**Prerequisites:** You don't need any prior knowledge about microelectronics, we'll explain everything! All you need are programming skills and computer literacy.
`,
            },
            {
                title: 'Zero-knowledge proofs',
                desc:
                    `ZKP is one of the most exciting and valuable theories behind modern Web3 systems. Over the last 4-5 years, the number of projects using ZKP has dramatically increased, and ZKP approaches evolved so that they could now be used almost everywhere: from creating proofs for assets transferring to solving scalability problems. It makes ZKP a new frontier of Web3 and decentralized systems.

This course aims to show the math behind ZKP and give some practical knowledge. We'll dive into modern ZKP frameworks, DSLs, lookup arguments techniques, zkEVM, and practical applications.`
            },
            {
                title: '10 calculi of Isabelle',
                desc:
                    `
It's nowadays fashionable to write about proof assistants based on dependent types. Mainly thanks to Kevin Buzzard's efforts to popularize the Lean Prover among "pure" mathematicians. This is good, but not good enough, since it overshadows systems based on higher-order logic and simply-typed λ-calculus; systems that have more libraries and industrial use-cases.

While Coq has CompCert to its credit, HOL4 verified the CakeML compiler, and Isabelle/HOL the seL4 microkernel. HOL4 was also used to formalize the semantics of x86, POWER, and ARM instruction sets. Lean 3 is famous for its Mathlib library, which systematically formalizes vast areas of modern mathematics, but in terms of breadth, it still falls short of the Archive of Formal Proofs — a catalogue of libraries for Isabelle/HOL, constantly updated and kept up-to-date.

This bias in popularity is all the more surprising given that dependent type theories are objectively more complex than higher-order logic. Moreover, dependent types are less amenable to automation, and tools like QuickChick and CoqHammer appeared several years after their Isabelle/HOL counterparts — Nitpick and Sledgehammer. The Isabelle/Isar language remains a poster child for writing structured, human-readable proofs.

**If you're already got interested in Isabelle/HOL, then half of this course's mission is accomplished :)**

The second half is to get intimately acquainted with the tools of formal language theory: calculi, their semantics, and meta-theorems.

To do this, we will start with an introduction to programming and proving properties of programs in Isabelle/HOL, and then move on to a leisurely examination of the paper "A Verified Decision Procedure for Orders in Isabelle/HOL". The paper gradually builds and verifies increasingly complex languages (calculi) for expressing judgments about the order relation between objects (something is greater than something else, but less than or equal to a third thing). The examples are simple, but not toy ones — ultimately, the verified decision procedure for order theory was incorporated into Isabelle/HOL itself, and now we can use it to automate proofs.

If the time permits, we'll get acquainted with relational semantics, a (homebrew) proof systems and their proof terms, and the refinement of declarative specifications into executable ones.
`,
            },
            {
                title: 'SAT / SMT',
                desc:
                    `
[SAT/SMT solvers](https://cacm.acm.org/magazines/2023/6/273222-the-silent-revolution-of-sat/fulltext) are heuristic engines for solving NP-complete problems in fields ranging from software and hardware verification to cryptography and bioinformatics. This course is about how they work inside: how to practically solve NP-complete problems.

From the solver's point of view, the task is to solve equations in a particular logic or theory. For example, in propositional logic, where only boolean variables and standard logical operators "and", "or", "not" and "implies" are used. Or in the logic of linear arithmetic, which includes logical operators, as well as arithmetic operations "+" and "multiplication by a constant". Using equations from a specific theory or their combination, one can attempt to crack a cipher or check the logic of a program for correctness.

The first problem that solvers started to solve is SAT, that is, the problem of determining the satisfiability of Boolean formulas. According to Cook-Levin theorem, this problem is NP-complete and most likely does not have a polynomial algorithm for its solution. However, this is not so bad: first, there are many heuristics and algorithms that speed up the brute-force solution of SAT tasks, and second, many theories are simply undecidable, that is, there is no algorithm that could find a solution in finite time.

**In this course we will:**

1. Explore the foundation of any solver - the algorithms of Conflict-Driven Clause Learning (CDCL) and Davis-Putnam-Logemann-Loveland (DPLL). The CDCL algorithm solves the SAT problem, and DPLL allows you to easily attach a resolution procedure to it, that is, an algorithm that solves the equation in a specific theory.
2. Consider various theories (linear arithmetic, bit vectors, arrays, pointers) and their decidability. If a theory is undecidable, we will try to find out the conditions under which it becomes decidable, and also study the corresponding resolution procedures.
3. Look at applications in [operations research](https://developers.google.com/optimization/cp), [cryptography](https://arxiv.org/abs/2108.04892) and [verification](http://linuxtesting.org/astraver).
`,
            },
            {
                title: 'Modern Lisps',
                desc:
                    `
Tired of waiting for the whole project to recompile just to test a minor change? Do you have an itch caused by the lack of a particular feature in your programming language? Have you always wanted to explore the magical world of Lisp but didn't know which dialect to pick or where to start? This course has answers to those questions and almost a [decade of professional Lisp experience](https://github.com/abcdw) to [share with you](https://savannah.gnu.org/project/memberlist.php?group=guix).

Functional programming is no surprise nowadays, but combining it with REPL and TDD to execute code, check it, and see the results as you type is not that common. All our practice will be highly interactive, programs are easily runtime-introspectable, and feedback loops are as short as possible.

In addition to REPLs, Lisps are also famous for their homoiconicity and macros, and during the course, we will do a lot of meta-programming: from adding missing language features to creating data-driven eDSLs and interpreters for them. We will embed one Lisp dialect into another, launch the same code in different runtimes, and do markup, styling, database querying, data transformation, all in a single language.

Of course, we will cover a brief history of Lisp language family, take a look at the basic building blocks, elegance and simplicity of the syntax, learn about what is so special about Lisp macros, discuss primary differences of various dialects, and try to understand which option is right for which task.

After that, we will go all the way down with Lisp: from configuring a text editor in Elisp and building a web application in Clojure(Script) to describing the whole operating system and deploying it to the server using Guile Scheme and Guix.`
            },
            {
                title: 'Static analysis',
                desc:
                    `
Static analyzers automatically check the properties of programs that interest us, without executing them. Typically, they are used to search for errors and vulnerabilities in programs, to improve their efficiency through optimizations, to help understand programs, and to perform refactoring.

We will take a small model imperative programming language based on Lua. Then we will implement an interpreter for it, and then a data flow analyzer using various abstract domains as examples. All of this we will do in the functional programming language OCaml or its dialect - Reason. Along the way, we will study all the necessary theory, as well as discuss practical details of implementation, in other words, how to write analyzer code succinctly, beautifully, and expansively.

**The topics from a theoretical standpoint that we will cover:**

- Basic concepts of static analysis
- Why creating a perfect static analyzer is actually impossible, but we are not giving up
- Syntax and semantics of programs
- Abstract interpretation
- Lattices, fixed point search algorithms, and their efficiency
- Monotonic frameworks for data flow analysis

**Prerequisites:**

What you don't necessarily need to know:

- Static analysis (it's a course on static analysis)
- Monads, lattices (we'll cover), and other jargon
- OCaml / Reason, Haskell, and other languages in the ML family (we'll learn)

It will be useful to have some basic experience with functional-style programming: be familiar with map/filter/fold.
`,
            },
        ],

        timetable_title: ['A typical day goes like'],
        timetable_content: [
            { name: 'Wake up' },
            { name: 'Morning yoga', kind: 'art', },
            { name: 'Breakfast' },
            { name: 'Seminar', kind: 'tech', },
            { name: 'Break' },
            { name: 'Lecture', kind: 'tech', },
            { name: 'Dinner' },
            { name: 'Dance class', kind: 'art', },
            { name: 'Break' },
            { name: 'Lecture', kind: 'tech', },
            { name: 'Snack time' },
            { name: 'Seminar', kind: 'tech', },
            { name: 'Break' },
            { name: 'Mountain biking', kind: 'art', },
            { name: 'Supper' },
            { name: 'Nighttime walk', kind: 'art', },
            { name: 'Prepare for sleep' },
            { name: 'Zzz...' },
        ],

        art_activities_title: 'Art activities',
        art_activities: [
            "Yoga",
            "Street and club dance classes",
            "Contemporary improvisation classes",
            "Lab group",
            "Contact improvisation",
            "Self-presentation",
            "Improvisation",
            "Listenings",
            "Thematic parties",
            "Game night",
            "Musical jams",
            "Die or survive competition",
            "Cringe day",
            "Evening promenade along the lake shore"
        ],

        venue_title: 'Venue and accomodation',
        venue_desc: [
            `The school's hosted at Hotels & Preference Hualing Tbilisi, a 5-star venue near Tbilisi Sea. The sea in question is a three minutes walk away.`,
            `The drive from Tbilisi airport takes 20 minutes, while a shuttle bus from Yerevan will get you there in about five hours.`,
        ],
        venue_presentation_link: 'Check out the venue presentation',

        till: 'till',

        registration_title: 'Participation cost',
        registration_sponsors: 'Sponsor\'s ticket',
        registration_desc:
            `**The cost covers:**
- Accomodation for 9 days and 8 nights
- Pool/sauna/fitness area access
- Three buffet meals + an afternoon snack a day
- A 24/7 snack station
- Participation in all school events
- and some swag

The only thing you have to fix yourself is getting to the venue. We'll help folks self-organize transfers from Vladikavkaz, Batumi or Yerevan. Once in Tbilisi, just hail a taxi.

You can share a room with another participant or book a single room: that's what xxxx/yyyy$ stand for. You can also get a fancy room with jacuzzi and sponsor grants for those who need them with a sponsor's ticket.

If you already live in Tbilisi and are comfortable with the commute, you can opt out of hotel accomodation.

We kindly ask corporate participants to pay more than the regular ticket. This helps us make the school more accessible for other participants.

**Select your participation type to see your ticket price:**`,

        personal: 'Personal ticket',
        corporate: 'Corporate ticket',
        i_live_in_tbilisi: 'I live in Tbilisi',
        accomodation_at_venue: 'Venue accomodation',

        cost_breakdown: 'Cost breakdown',
        revenue: 'Revenue',
        opex: 'Expenses',
        profit: 'Profit',
        budget_excluding_accomodation: 'Budget (excluding accomodation)',
        check_out_cost_breakdown: 'Full cost breakdown and transparent accounting',

        apply_title: 'Apply now',
        apply_grant: 'Apply for a grant',
        apply_course: 'Submit a course',
    },
    ru: {
        headline: ['Формальные методы', 'Машинное обучение', 'Сольфеджио', 'FPGA', 'Криптография', 'Системное программирование', 'Функциональное программирование', 'Искусство'],
        gamarjoba: 'Гамарджоба',
        tagline: 'Летняя школа сложного программирования и',
        tagline_contemporary: 'современного искусства',
        headline_2: (count: string) => `Одноранговая вечеринка${count ? ` на ${count}` : ''}. Инженерия и задор.`,
        duration_info: ['40ч курсов', '30ч искусства'],
        time: ['15-23 июля'],
        place: ['близ Тбилиси'],

        pic_titles: ['Выбери фулл-тайм курс', 'Побудь с творческой программой', 'Приезжай'],

        names_and_faces: 'Кто все эти люди',

        courses_header: 'Курсы (предварительные)',
        courses: [
            {
                title: 'Семинары по ML',
                desc:
                    `
Этот курс подойдёт для R&D-исследователей из других областей, которым интересно, что происходит, но неинтересно отличать кошечек от собак с помощью \`fit_predict()\`, и для настоящих машинлёрнеров, которые чувствуют себя окопавшимися в своей подобласти (CV, например) и хотят узнать, что за туса в других частях. (Подготовить и рассказать свою классную статью очень приветствуется!)

Курс будут вести несколько семинаристов, шарящих в своих областях, и работать он будет так: мы будем читать статьи, вместе обсуждать и расписывать их, пока не станут понятны все буквы, которые нас интересуют, и соединять в нарративы того, куда всё идет или шло.

В отличие от других курсов на школе, в которых могут учить делать что-то руками методом лекций и задачек, этот направлен скорее на концептуальное понимание (мы будем читать и может быть выводить формулы, но писать сколь-либо много кода не будем) и горизонтальность (если вы шарите за X – расскажите, у нас семинар).`,
            },
            {
                title: 'Purescript',
                desc:
                    `
PureScript — чисто-функциональный язык, заимствующий многие идеи у Haskell, но предназначенный для компиляции в JavaScript.

**На курсе вы получите:**
- краткое введение в функциональное программирование
- немного персональной практики
- коллективное исследование языка — написание нашей собственной реализации Virtual DOM

**Курс будет разбит на две последовательные части:**
- Основы: введение в PureScript и ФП. Чистые функции, эффекты, монады, синтаксис языка.
- Практика: хакинг VDOM. Посмотрим, что внутри у фреймворка Halogen. Реализация своих diff и patch. Обход деревьев. Взаимодействие PureScript с JavaScript.

Можно начать с первой, чтобы аккуратно погрузиться, или сразу нырнуть в VDOM.
`,
            },
            {
                title: 'Приключения Логических Интегральных Схем',
                desc:
                    `
Программирование настолько сильно завязано на семантику фон-Неймановской машины, что многим просто не приходит в голову, что алгоритм можно реализовать не только цепочкой инструкций, несколькими ядрами с несколькими цепочками, или микрокодом с генерацией из цепочки квази-инструкций контрольных сигналов.

На самом деле CPU и все его варианты — это очень частный случай того, что можно сделать в железе. Для того, чтобы понять, что можно сделать в железе, на него нужно смотреть не в терминах программирования (циклы, вызовы функций и т.д.), а в других, аппаратных терминах: комбинационное облако и элементы состояния, конвейеры и очереди, такты и задержки внутри такта, определяющие тактовую частоту.

В последние 30 лет проектировщики микросхем не рисуют цифровые схемы мышкой на экране, а синтезируют их кодом на языке описания аппаратуры: Verilog или VHDL. Этот код внешне поход на программы, но имеет другую семантику, которую мы и покажем в этом курсе. Когда код разработан, его можно синтезировать в дизайн физической микросхемы (так делают новые схемы) или превратить в граф из логических элементов и прошить в память FPGA.

**FPGA - это микросхема, на которой соединения транзисторов программируется.**

Прошив её данным кодом, мы получим аппаратный ускоритель медленнее специально изготовленной узконаправленной схемы, но для многих задач быстрее/энергоэффективнее, чем обычный CPU с эквивалентной программой. Так прототипируют новые процессоры и микросхемы и разрабатывают аппаратные ускорители.

Помимо простых лаб с FPGA мы покажем как строить RISC-V процессор и применять FPGA для обработки звука. Посмотрим на FLOSS-средство проектирования ASIC Open Lane — альтернативу коммерческим пакетам от Synopsys и Cadence.

Наконец, мы свяжем другие темы школы, такие как формальные методы и решатели ограничений, с их применением в электронных компаниях. В этих компаниях доказательства утверждений темпоральной логики применяются для верификации конвейеров, а решатели ограничений используются для генерации непротиворечивых псевдослучайных транзакций при функциональной верификации через симуляцию.

**План курса:**
  
- День 1. Комбинаторная логика - вычисления без тактов. Языки описания аппаратуры, маршруты ASIC и FPGA, а также жизнь в электронной индустрии.
- День 2. Последовательностная логика, тактовый сигнал и конечные автоматы. Применение для распознавания музыки.
- День 3. Две стороны процессора - архитектура и микроархитектура. Разбор минималистичного ядра schoolRISCV.
- День 4. Против лома нет приема: почему специализированный ASIC на порядки быстрее CPU. Конвейеры, очереди, встроенные блоки памяти и вопросы тактовой частоты. Применения для GPU, сетевых чипов и ускорителей машинного обучения.
- День 5. Формальная верификация, применение С++ для разработки проектов на FPGA и на десерт: разбор примера с процессором внутри FPGA, подключением к VGA и игрой тетрис.

**Пререквизиты:** можно ничего не знать о микроэлектронике, всё расскажем! Нужно уметь программировать и обладать компьютерной грамотностью.
`,
            },
            {
                title: 'Zero-knowledge proofs',
                desc:
                    `ZKP - одна из самых захватывающих и ценных теорий, стоящих за Web3. За последние 4-5 лет количество проектов, использующих ZKP, резко возросло -- их можно использовать практически везде: от создания доказательств передачи активов до решения проблем масштабируемости. Это делает ZKP новым фронтиром Web3 и децентрализованных систем.

Цель этого курса - показать математику, стоящую за ZKP, и предоставить некоторые практические знания. Мы погрузимся в современные фреймворки и DSL ZKP и посмотрим на практические применения.

Требования: базовые знания абстрактной алгебры (группы, генераторы групп), криптографии (цифровые подписи) и алгоритмов. Знание языка программирования Rust (умение читать и писать код с трейтами Fn*) приветствуется.`
            },
            {
                title: '10 исчислений Isabelle',
                desc:
                    `
Сейчас модно писать про системы доказательства теорем, основанные на зависимых типах. В основном так вышло благодаря усилиям Кевина Баззарда по популяризации Lean Prover среди "чистых" математиков. Это хорошо, но недостаточно хорошо, поскольку обходит вниманием системы, основанные на логике высшего порядка и просто-типизированном λ-исчислении; системы, у которых больше библиотек и индустриальных кейсов.

В то время как на счету Coq имеется CompCert, на HOL4 верифицировали компилятор CakeML, а на Isabelle/HOL — микро-ядро seL4. Тот же HOL4 использовали для формализации семантики инструкций x86, POWER и ARM. Lean 3 знаменит библиотекой Mathlib, систематически формализующей обширные области современной математики, но по широте охвата она всё ещё уступает Archive of Formal Proofs — каталогу библиотек для Isabelle/HOL, постоянно пополняемому и поддерживаемому в актуальном состоянии.

Такой перекос в известности тем удивительнее, что теории зависимых типов объективно сложнее логики высшего порядка. Более того, зависимые типы хуже поддаются автоматизации, и такие инструменты как QuickChick и CoqHammer появились через несколько лет после своих аналогов для Isabelle/HOL — Nitpick и Sledgehammer. А язык Isabelle/Isar остаётся образцом для записи структурированных, человекочитаемых доказательств.

**Если вам уже интересна Isabelle/HOL, то половину своей задачи курс выполнил :)**

Вторая половина — тесно познакомиться с инструментами формальной теории языков программирования: исчислениями, их семантикой и метатеоремами.

Для этого начнём с введения в программирование и доказательство свойств программ в Isabelle/HOL, после чего перейдём к неспешному разбору статьи "A Verified Decision Procedure for Orders in Isabelle/HOL". Статья постепенно строит и верифицирует всё более и более сложные языки (исчисления) для выражения суждений об отношении порядка между объектами (что-то больше, чем что-то другое, но меньше или равно чему-то третьему). Примеры простые, но не игрушечные — в конечном итоге верифицированная разрешающая процедура для теории порядков была включена в саму Isabelle/HOL, и теперь мы можем её использовать для автоматического получения доказательств.

Если времени хватит на всё, то мы приобщимся к таким явлениям как реляционная семантика, (доморощенная) система доказательств и её пруф-термы, и уточнение декларативных спецификаций до исполнимых.

**Пререквизиты**

- Базовое функциональное программирование
- Основы формальной логики
`
            },
            {
                title: 'SAT / SMT',
                desc:
                    `
[SAT/SMT солверы](https://cacm.acm.org/magazines/2023/6/273222-the-silent-revolution-of-sat/fulltext) — эвристический движок решения NP-полных задач в областях от верификации софта и железа до криптографии и биоинформатики. Этот курс — про то, как они работают внутри: как практически решать NP-полные задачи.

С точки зрения солвера, задача заключается в решении уравнений в определенной логике или теории. Например, в пропозиционной логике, где используются только булевы переменные и стандартные логические операторы "и", "или", "не" и "следует". Или в логике линейной арифметики, которая включает логические операторы, а также арифметические операции "+" и "умножение на константу". С помощью уравнений из конкретной теории или их комбинации можно попытаться взломать шифр или проверить логику программы на корректность.

Первая задача, которую начали решать солверы, это SAT, то есть задача определения выполнимости булевых формул. По теореме Кука-Левина эта задача является NP-полной и, скорее всего, не имеет полиномиального алгоритма для ее решения. Однако это не так уж плохо: во-первых, существует множество эвристик и алгоритмов, которые ускоряют переборное решение SAT-задачи, а во-вторых, многие теории просто не разрешимы, то есть не существует алгоритма, который мог бы найти решение за конечное время.

**В этом курсе мы:**
1. Изучим основу любого солвера - алгоритмы конфликтно-ориентированного обучения дизъюнктам (CDCL) и Дэвиса-Патнема-Логемана-Лавленда (DPLL). Алгоритм CDCL решает SAT-задачу, а DPLL позволяет легко присоединить к нему разрешающую процедуру, то есть алгоритм, который решает уравнение в определенной теории.
2. Рассмотрим различные теории (линейную арифметику, битовые векторы, массивы, указатели) и их разрешимость. Если теория не разрешима, то мы постараемся выяснить ограничения, при которых она становится разрешимой, а также изучим соответствующие разрешающие процедуры.
3. Посмотрим на применения в [operations research](https://developers.google.com/optimization/cp), [криптографии](https://arxiv.org/abs/2108.04892) и [верификации](http://linuxtesting.org/astraver).
`
            },
            {
                title: 'Современные Лиспы',
                desc:
                    `
Грустно ждать пересборки всего проекта, чтобы протестировать небольшое изменение? Не хватает фичей языка? Всегда хотелось нырнуть в Лиспы, но было непонятно, какой взять диалект и откуда начать? Этот курс про ответы на эти вопросы и [много лет](https://github.com/abcdw) опыта Лиспов в [индустрии](https://savannah.gnu.org/project/memberlist.php?group=guix).

ФП уже никого не удивить, но с REPL и TDD, чтобы сразу тестировать и видеть результат, его совмещают не так часто. Вся наша практика будет очень интерактивная, программы интроспектируемые в рантайме, и циклы обратной связи суперкороткими.

Кроме REPLов, Лиспы также известны своей гомоиконичностью и макросами, и в ходе курса мы будем активно метапрограммировать: добавлять новые фичи в язык и делать data-driven eDSL и интерпретаторы к ним. Будем вкладывать одни диалекты Лиспов в другие, запускать один код на разных рантаймах, делать разметку, вёрстку, запросы к БД, манипуляцию данных – всё в одном языке.

Также мы кратко пройдём по истории семейства Лиспов и посмотрим, какие фичи идеального Лиспа сейчас есть в разных диалектах.

В конце концов, мы нырнём в скобочный мир с головой: настроим IDE Лиспом, сделаем веб-приложение Лиспом, опишем ОС Лиспом, и задеплоим тоже Лиспом (Elisp, Clojure(Script), Guile Scheme, Guix).
`,
            },
            {
                title: 'Статический анализ',
                desc:
                    `
Статические анализаторы автоматически проверяют интересующие нас свойства программ, не выполняя их. Как правило, они используются, чтобы искать ошибки и уязвимости в программах, повышать их эффективность путем оптимизаций, помогать понимать программы и выполнять рефакторинг.

Мы возьмем небольшой модельный императивный язык программирования на базе Lua. Далее мы реализуем для него интерпретатор, а после — анализатор потоков данных на примере различных абстрактных доменов. Все это мы будем делать на языке функционального программирования OCaml или его диалекте — Reason. В процессе мы будем изучать всю необходимую теорию, а также обсуждать практические детали реализации, иными словами, как написать код анализатора достаточно лаконично, красиво и расширяемо.

**Темы с точки зрения теории, которые мы разберем:**

- Основные понятия статического анализа
- Почему идеальный статический анализатор на самом деле создать невозможно, но при этом мы не опускаем руки
- Синтаксис и семантика программ
- Абстрактная интерпретация
- Решетки, алгоритмы поиска неподвижной точки и их эффективность
- Монотонные фреймворки для анализа потоков данных

**Пререквизиты:**

Чего знать не обязательно:
- Статический анализ (собственно, зачем, если мы на курсе и будем изучать его основы)
- Монады, решетки (узнаем) и прочие словечки
- OCaml / Reason, Haskell и прочие языки семейства ML (научимся)

Однако, при этом желательно иметь какой-то базовый опыт программирования в функциональном стиле. Как минимум не бояться операций map, filter и fold над списками.
`,
            },
        ],

        timetable_title: ['Обычный день проходит так'],

        timetable_content: [
            { name: 'Просыпание' },
            { name: 'Утренняя йога', kind: 'art', },
            { name: 'Завтрак' },
            { name: 'Семинар', kind: 'tech', },
            { name: 'Перерыв' },
            { name: 'Лекция', kind: 'tech', },
            { name: 'Обед' },
            { name: 'Класс по танцам', kind: 'art', },
            { name: 'Перерыв' },
            { name: 'Лекция', kind: 'tech', },
            { name: 'Полдник' },
            { name: 'Семинар', kind: 'tech', },
            { name: 'Перерыв' },
            { name: 'Горные велосипеды', kind: 'art', },
            { name: 'Ужин' },
            { name: 'Ночная прогулка', kind: 'art', },
            { name: 'Подготовка ко сну' },
            { name: 'Zzz...' },
        ],

        art_activities_title: 'Арт-программа',
        art_activities: [
            "Йога",
            "Классы по уличным и клубным стилям",
            "Классы contemporary импровизации",
            "Группа лаборатория",
            "Контактная импровизация",
            "Самопрезентация",
            "Импровизация",
            "Лисэнинги",
            "Тематические вечеринки",
            "Вечер с играми",
            "Музыкальные джемы",
            "Конкурс сдохни или умри",
            "Cringe day",
            "Вечерний променад по берегу озера"
        ],

        venue_title: 'Площадка и размещение',
        venue_desc: [
            `Школа пройдёт в Hotels & Preference Hualing Tbilisi, пятизвёздочном отеле у Тбилисского Моря. Море в трёх минутах ходьбы.`,
            `Дорога от аэропорта занимает двадцать минут. На автобусе из Еревана можно добраться за пять-шесть часов.`
        ],
        venue_presentation_link: 'Презентация площадки',

        till: 'до',

        registration_title: 'Стоимость участия',
        registration_sponsors: 'Спонсорский билет',
        registration_desc: `**Цена включает:**
- Проживание 9 дней и 8 ночей
- Басейн/сауну/спортзал
- Шведский стол три раза в день + полдники
- Круглосуточные перекусы
- Участие во всех мероприятиях школы

Самостоятельно нужно только добраться. Мы поможем участникам самоорганизовать трансферы из Владикавказа/Батуми/Еревана. Добравшись до Тбилиси, можно просто вызвать такси.

На школе можно жить вместе с другим участником или одному: это то, что значат xxxx/yyyy$. Ещё можно забронировать моднейший номер с джакузи и проспонсировать гранты для других участников, взяв спонсорский билет.

Если вы уже живётё в Тбилиси и вам комфортно ездить на школу, вы можете не платить за проживание в отеле.

Мы просим корпоративных участников платить больше за билет. Это позволяет нам сделать школу более доступной для других участников.

**Чтобы посмотреть стоимость своего билета, выберите свой тип участия:**`,
        personal: 'Личный билет',
        corporate: 'Корпоративный билет',
        i_live_in_tbilisi: 'Я живу в Тбилиси',
        accomodation_at_venue: 'Размещение в отеле',

        cost_breakdown: 'Составляющие стоимости',
        revenue: 'Доход',
        opex: 'Затраты',
        profit: 'Прибыль',
        budget_excluding_accomodation: 'Бюджет (без отеля)',
        check_out_cost_breakdown: 'Полный расчёт бюджета и прозрачная бухгалтерия',

        apply_title: 'Регистрация',
        apply_grant: 'Заявка на грант',
        apply_course: 'Предложить курс',
    }
};
