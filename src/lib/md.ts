import { marked } from 'marked';
export const md = (s: string) => marked.parse(s, { smartypants: true });
