import type { LayoutLoad } from './$types';
import i18n from '$lib/i18n';

export const load: LayoutLoad = ({ params }) =>
({
    lang: params.lang || 'en',
    content: { ...i18n.en, ...i18n[params.lang || 'en'] }
})