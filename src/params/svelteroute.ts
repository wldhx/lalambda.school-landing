export function match(param: string): boolean {
    return !/^(21|22)$/.test(param);
}
