## Участникам

Летняя школа Лялямбда-2021 пройдёт **с 17 июля по 1 августа** в [Парк-отеле Ершово](https://ershovo.su/) (Московская обл., Одинцовский г.о., с. Ершово, кв-л Дома отдыха Ершово, стр.22). Наша группа проживает в корпусе №3. В стоимость проживания включено трёхразовое питание по системе шведский стол, в стоимость оргсбора включены полдники и вечерние перекусы на свечке.

Нами запланирован **организованный трансфер**:

- туда 17 июля утром (суббота)
- туда 24 июля утром (суббота)
- обратно 28 июля вечером (среда)
- обратно 1 августа вечером (воскресенье)

### Материалы пре-школы

К школе можно подтянуть свои знания, пройдясь по указанным ниже методичкам. По ходу можно задавать вопросы в чате участников, кто-нибудь обязательно подскажет.

- [Как делать воркшопы](https://docs.google.com/document/d/1YW7AWWsfaLsI6_I4caPfJKZxQFqEusNJX8_G3-exMgg/edit#)

#### ФП

- [Курс Москвина про Haskell](https://stepik.org/course/75/promo)

#### Coq

- [Курс Антона по Coq](https://github.com/anton-trunov/csclub-coq-course-spring-2021) (+[лекции](https://www.youtube.com/watch?v=262F_GhRyv4) на YouTube)

- [Logical Foundations](https://softwarefoundations.cis.upenn.edu/lf-current/index.html) (+[лекции](https://deepspec.org/event/dsss18/videos.html) ("Coq intensive") с DSSS)

#### TLA

- [Книга Specifying Systems](http://lamport.azurewebsites.net/tla/book-02-08-08.pdf) и [упражнения](http://lamport.azurewebsites.net/tla/web.zip) к ней
- [Лекции Лампорта](http://lamport.azurewebsites.net/video/videos.html)

### Подготовка ЭВМ

Тестовые программы для проверки окружения: <https://git.sr.ht/~omrigan/lalambda-env>.

#### Coq

- Дима советует: заведите VSCode + VSCoq.
- Антон советует: пользуйте Emacs + Proof General.
- Олег бурчит: если ничего не работает, сработает [CoqIDE из Coq platform](https://coq.inria.fr/download).

#### TLA+

Установите [Java 16](https://adoptopenjdk.net/) и [TLA+ Toolbox](https://lamport.azurewebsites.net/tla/toolbox.html).

#### Haskell

Установите [Stack](https://docs.haskellstack.org/en/stable/README/) и запустите `stack ghci`.
