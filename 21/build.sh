#!/bin/sh
pandoc ru.yml -f markdown \
	--template index.md \
	| pandoc - ru.yml -f markdown --template template.pandoc --css index.css -s -o build/index.html
pandoc participants.md \
	| pandoc - ru.yml -f markdown --template template.pandoc --css index.css -s -o build/participants.html
pandoc fv.md \
	| pandoc - ru.yml -f markdown --template template.pandoc --css index.css -s -o build/fv.html
pandoc en.yml -f markdown \
	--template en.md \
	| pandoc - en.yml -f markdown --template template.pandoc --css index.css -s -o build/en.html
